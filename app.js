Ext.application({
	name     : 'Gnt.examples.msproject_import',
	//appFolder: 'app',
	requires : [
		'Gnt.examples.msproject_import.view.MSProjectGantt'
		],
		launch   : function() {
			new Ext.Viewport({
				layout: 'border',

				items: [
					Ext.create('Gnt.examples.msproject_import.view.MSProjectGantt'),
					{
						xtype: 'details'
					}
					]
			});
		}
});
