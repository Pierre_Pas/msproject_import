Ext.define('Gnt.examples.msproject_import.view.MSProjectGantt', {
    extend : 'Gnt.examples.lib.GanttPanel',

    requires : [
        'Ext.form.Panel',
        'Ext.form.TextField',
        'Ext.form.File',
        'Gnt.model.Task',
        'Gnt.data.TaskStore',
        'Gnt.column.Name',
        'Gnt.column.StartDate',
        'Gnt.column.EndDate',
        'Gnt.column.ResourceAssignment',
        'Gnt.column.PercentDone',
        'Gnt.column.AddNew',
        'Gnt.examples.msproject_import.model.MSProjectTask',
        'Gnt.examples.msproject_import.data.ux.Importer',
        'Gnt.examples.msproject_import.view.ux.MSImportPanel'
    ],

    region            : 'center',
    title             : 'Loading data from MS Project',
    border            : false,
    bodyBorder        : false,
    stripeRows        : true,
    rowHeight         : 31,
    leftLabelField    : {
        dataIndex : 'Name',
        editor    : { xtype : 'textfield' }
    },
    highlightWeekends : true,
    showTodayLine     : true,
    loadMask          : true,
    startDate         : new Date(2012, 4, 1),
    viewPreset        : 'weekAndDayLetter',

    lockedGridConfig : { width : 200 },

    //static column that will be removed when columns from mpp file are loaded
    columns : [
        {
            xtype : 'namecolumn',
            width : 200
        }
    ],

    initComponent : function() {
        var me = this;

        var taskStore = new Gnt.data.TaskStore({
            model : 'Gnt.examples.msproject_import.model.MSProjectTask',
            root  : {
                children : [
                    {
                        Name      : 'Hello World',
                        StartDate : new Date(2012, 4, 1),
                        EndDate   : new Date(2012, 4, 3),
                        leaf      : true
                    }
                ]
            }
        });

        var importer = new Gnt.examples.msproject_import.data.ux.Importer();

        Ext.apply(me, {
            taskStore : taskStore,
            plugins   : importer,
            endDate   : Sch.util.Date.add(new Date(2012, 4, 1), Sch.util.Date.WEEK, 20),

            tbar : [
                {
                    xtype     : 'msimportpanel',
                    listeners : {
                        dataavailable : function(form, data) {
                            Ext.Msg.alert('Success', 'Data from .mpp file loaded');

                            importer.importData(data);

                            me.lockedGrid.reconfigure(me.lockedGrid.getStore(), data.columns.concat({ xtype : 'addnewcolumn' }));

                            me.expandAll();

                            var span = me.taskStore.getTotalTimeSpan();
                            if (span.start && span.end) {
                                me.setTimeSpan(span.start, span.end);
                            }
                        }
                    }
                }
            ]
        });

        me.callParent();
    }
});